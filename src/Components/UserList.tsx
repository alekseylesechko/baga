import React, {FC} from 'react';
import {IUser} from '../types/types';

interface Props {
	users: any[];
}

const UserList: FC<Props> = ({users}) => {
	return (
		<div>
			{users.map((user) => (
				<div key={user.id} style={{padding: '15px', border: '1px solid black'}}>
					{user.id}. {user.name} lived in {user.address.city}
				</div>
			))}
		</div>
	);
};

export default UserList;
