import React, {FC, ReactElement, useEffect} from 'react';

interface Props {
	type: number;
}

const Component: FC<Props> = ({type}): ReactElement => {
	useEffect(() => {
		console.log('Hello', type); // just one time
	}, []);

	useEffect(() => {
		console.log('Bye', type);
	}, [type]);

	return <div>New Component</div>;
};

export default Component;
