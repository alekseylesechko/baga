import React, {FC, ReactElement} from 'react';

export enum CardVariant {
	outLined = 'outLined',
	primary = 'primary',
}

interface Props {
	width: string;
	height: string;
	variant?: CardVariant;
	children?: React.ReactChild | React.ReactNode;
	onClick: () => void;
}

const Card: FC<Props> = ({
	width,
	height,
	children,
	variant = CardVariant.primary,
	onClick,
}): ReactElement => {
	return (
		<div
			style={{
				width,
				height,
				display: 'inline-block',
				border: variant === CardVariant.outLined ? '1px solid red' : 'none',
				background: variant === CardVariant.primary ? '#ccc' : '',
			}}
			onClick={onClick}
		>
			{children}
		</div>
	);
};

export default Card;
