import React, {useEffect, useState} from 'react';
import './App.css';
import Card, {CardVariant} from './Components/Card';
import Component from './Components/Component';
import UserList from './Components/UserList';
import {IUser} from './types/types';

function App() {
	const [users, setUsers] = useState<IUser[]>([]);

	function getUsers() {
		try {
			fetch('https://jsonplaceholder.typicode.com/users')
				.then((response) => response.json())
				.then((json) => setUsers(json));
		} catch (e) {
			console.log(e);
		}
	}

	useEffect(() => {
		getUsers();
	}, []);

	const [num, setNum] = useState(0);

	function onClick() {
		console.log('click');
	}

	return (
		<div className="App">
			<Card
				width="200px"
				height="200px"
				variant={CardVariant.outLined}
				onClick={onClick}
			>
				<button>Add</button>
			</Card>
			<button onClick={() => setNum((n) => n + 1)}>Click me</button>
			<UserList users={users}></UserList>
			{num === 0 ? (
				<Component type={1}></Component>
			) : (
				<Component type={2}></Component>
			)}
		</div>
	);
}

export default App;
